import React from "react";
import axios from 'axios';
import { Redirect } from "react-router-dom";
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardText,
  FormGroup,
  Form,
  Input,
  Row,
  Col
} from "reactstrap";

class PackageDetails extends React.Component{
  constructor(props){
    super(props)

    this.submit=this.submit.bind(this)
  }
  submit(){
    console.log(this.props.shipmentId)
  }
  render(props){
    return (
      <>
      <div>
        <Card>
          <CardHeader>
            <h5 className="title">New Package Details</h5>
          </CardHeader>
          <CardBody>
              <Row>
                  <label> Shipment ID : {this.props.shipmentId}</label>
              </Row>
              <Row>
                <label>Receipt Number : {this.props.receiptNo}</label>
              </Row>
              <Row>
                <label> Tracking Number : {this.props.trNo}</label>
              </Row>
              <Row>
                <label> Customer : {this.props.customer}</label>
              </Row>

            <h5> Warehouse Receipt Details</h5>
            <Row>
              <label> Inland Carrier : {this.props.inLandCarrier}</label>
            </Row>
            <Row>
              <label> Shipper : {this.props.shipper}</label>
            </Row>
            <Row>
              <label> Date : {this.props.date}</label>
            </Row>

            <h5>Cargo Details</h5>
            <Row>
              <label> Pieces : {this.props.pieces}</label>
            </Row>

          </CardBody>
          <Button className="btn-fill" color="secondary" onClick={this.submit}>
          Create Package
          </Button>
        </Card>
        </div>
      </>
    )
  }
}
export default PackageDetails;
