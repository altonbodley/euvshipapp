import React from "react";
import axios from 'axios';
import { Redirect } from "react-router-dom";

import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardText,
  FormGroup,
  Form,
  Input,
  Row,
  Col
} from "reactstrap";

class NewCustomer extends React.Component{
  constructor(props){
    super(props)
    this.state={error:false,email:'',fname:'',lname:'',addressl1:'',addressl2:'',city:'',
      parish:'',contact:'',trn:'',token:sessionStorage.getItem("token")};
    this.changeState=this.changeState.bind(this);
    this.submit=this.submit.bind(this);
    this.viewCustomers=this.viewCustomers.bind(this);
  }
  changeState(event){
    this.setState({
      [event.target.name]:event.target.value
    });
  }
  viewCustomers(e){
    e.preventDefault();
    this.props.history.push("/euvasal/customer");
  }
  submit(e){
    e.preventDefault();
    const {email,fname,lname,addressl1,addressl2,
      city,parish,contact,trn,token}=this.state;
    let newCustomer={"email":email,"fname":fname,"lname":lname,"addressl1":addressl1,"addressl2":addressl2,
      "city":city,"parish":parish,"contact":contact,"trn":trn}
    console.log(newCustomer);
    axios.post('http://0.0.0.0:5000/new_customer',newCustomer,{auth:{username:token}}).then(
      (result)=>{
        console.log(result.status);
        if (result.status==201){
          console.log("Customer created")
          this.props.history.push("/euvasal/customer");
        }
      }
    )
  }
  render(){
    return(
      <>
        <div className="content">
          <Row>
            <Col md="10">
              <Card>
                <CardHeader>
                  <h5 className="title">Add New Customer</h5>
                </CardHeader>
                <CardBody>
                  <Form onSubmit={this.submit}>
                    <Row>
                      <Col className="pl-md-1" md="8">
                        <FormGroup>
                          <label>Email address</label>
                          <Input
                            name="email"
                            type="email"
                            onChange={this.changeState}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col className="pr-md-1" md="6">
                        <FormGroup>
                          <label>First Name</label>
                          <Input
                            name="fname"
                            type="text"
                            onChange={this.changeState}
                          />
                        </FormGroup>
                      </Col>
                      <Col className="pl-md-1" md="6">
                        <FormGroup>
                          <label>Last Name</label>
                          <Input
                            name="lname"
                            onChange={this.changeState}
                            type="text"
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col md="12">
                        <FormGroup>
                          <label>Address line 1</label>
                          <Input
                            type="text"
                            name="addressl1"
                            onChange={this.changeState}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col md="12">
                        <FormGroup>
                          <label>Address line 2</label>
                          <Input
                            type="text"
                            name="addressl2"
                            onChange={this.changeState}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col className="pr-md-1" md="4">
                        <FormGroup>
                          <label>City</label>
                          <Input
                            name="city"
                            onChange={this.changeState}
                            type="text"
                          />
                        </FormGroup>
                      </Col>
                      <Col className="px-md-1" md="7">
                        <FormGroup>
                          <label>Parish</label>
                          <Input
                            type="text"
                            name="parish"
                            onChange={this.changeState}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col className="pl-md-1" md="4">
                        <FormGroup>
                          <label>Contact</label>
                          <Input
                            type="number"
                            name="contact"
                            onChange={this.changeState}
                          />
                        </FormGroup>
                      </Col>
                      <Col md="4">
                        <FormGroup>
                          <label>TRN</label>
                          <Input
                            type="number"
                            name="trn"
                            onChange={this.changeState}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Button className="btn-fill" color="secondary" type="submit">
                      Add Customer
                    </Button>
                    <Button onClick={this.viewCustomers} className="btn-fill" color="primary">View Customers</Button>
                  </Form>
                  <Row >
                    <ul>
                      <li id={this.state.error ? "errorVis":"error"}>This form has errors</li>
                    </ul>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </>
    );
  }
}
export default NewCustomer;
