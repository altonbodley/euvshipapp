import React from "react";
import axios from 'axios';
import ReactTable from 'react-table';
import 'react-table/react-table.css'
import update from 'immutability-helper';
// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Table,
  Row,
  Col,
  Button
} from "reactstrap";

class Customer extends React.Component{
  constructor(props){
    super(props)
    this.state={token:sessionStorage.getItem("token"),names:[]};
    console.log("loaded");
    this.newCustomer=this.newCustomer.bind(this);
  }
  componentDidMount(){
    const {names}=this.state;
    axios.get('http://0.0.0.0:5000/get_all_customers',{auth:{username:this.state.token}}).then(
      (result)=>{
        console.log(result.data)
        this.setState({names:result.data})
        console.log(this.state.names);
      })
  }
  newCustomer(e){
    e.preventDefault()
    this.props.history.push("/euvasal/new_customer");
  }

  render(){
    var data=this.state.names
    const columns=[
      {
      Header: 'First Name',
      accessor:'fname'
    },{
      Header:'Last Name',
      accessor:'lname'
    },{
      Header:'E-mail',
      accessor:'email'
    },{
      Header:'Adderess 1',
      accessor:'addressl1'
    },{
      Header:'Address 2',
      accessor:'addressl2'
  },{
    Header:'City',
    accessor:'city'
  },{
    Header:'Parish',
    accessor:'parish'
  },{
    Header:'Contact',
    accessor:'contact'
  },{
    Header:'TRN',
    accessor:'trn'
  }
    ]
    return(
      <>
      <div className="content">
        <Row>
          <Col md="12">
            <Card>
              <CardHeader>
                <CardTitle tag="h4">Customers</CardTitle>
              </CardHeader>
              <CardBody>
              <ReactTable
              data={data}
              columns={columns}
              resolveData={data=>data.map(row=>row)}/>
              </CardBody>
              <Col md='3'>
                <Button onClick={this.newCustomer} className="btn-fill" color="primary">New Customer</Button>
              </Col>
            </Card>
          </Col>
        </Row>
      </div>
      </>
    );
  }
}
export default Customer;
