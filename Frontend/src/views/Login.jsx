import React from "react";
import axios from 'axios';
import { withAlert } from 'react-alert';
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardText,
  FormGroup,
  Form,
  Input,
  Row,
  Col
} from "reactstrap";

class Login extends React.Component{
  constructor(props){
    super(props);
    this.state={
      error:false,
      email:'',
      password:''
    };
    this.changeState=this.changeState.bind(this);
    this.submit=this.submit.bind(this);
  }



  validateForm(){
    return this.state.username.length >5 && this.state.password.length>0
  }
  changeState(event){
    this.setState({
      [event.target.name]:event.target.value
    });
  }
  componentWillUnmount() {
    
  }
  submit(e){
    e.preventDefault();
    const {username,password}=this.state;
    let data={
      "username":username,
      "password":password
    }
    console.log(data)
    console.log(username,password);
    axios.post('http://0.0.0.0:5000/login',data).then(
      (result)=>{
        console.log(result.status);
        if (result.status!=202){
          console.log(result.data.error);
          this.props.alert.error("Oops, Check your username and password!");
          this.setState({error:true,password:null});
          if(sessionStorage.getItem("auth")==true || sessionStorage.getItem("auth")==null){
            sessionStorage.removeItem("auth");
            sessionStorage.removeItem("token");
          }
        }else{
          sessionStorage.setItem("auth",true);
          sessionStorage.setItem("token",result.data);
          this.props.history.push("/euvasal/dashboard");
        }
      }
    )
  }

  render(){
    return(
      <>
        <div className="content" id="login">
          <Row>
            <Col md="4">
              <Card>
                <CardHeader>
                  <h5 className="title">Login</h5>
                </CardHeader>
                <CardBody>
                  <Form onSubmit={this.submit}>
                    <Row>
                      <Col className="pr-md-1" md="12">
                        <FormGroup>
                          <label>Username</label>
                          <Input
                            name="username"
                            placeholder="johnDoe"
                            type="text"
                            onChange={this.changeState}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col className="pr-md-1" md="12">
                        <FormGroup>
                          <label>Password</label>
                          <Input
                            name="password"
                            type="password"
                            onChange={this.changeState}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Button className="btn-fill" color="secondary" type="submit">
                      login
                    </Button>
                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </>
    );
  }
}
export default withAlert(Login);
