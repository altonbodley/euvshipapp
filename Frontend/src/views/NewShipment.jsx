import React from "react";
import axios from 'axios';
import { Redirect } from "react-router-dom";
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardText,
  FormGroup,
  Form,
  Input,
  Row,
  Col
} from "reactstrap";

class NewShipment extends React.Component{
  constructor(props){
    super(props)
    this.state={token:sessionStorage.getItem("token"),origin:'',destination:'',shipmentId:''};
    this.changeState=this.changeState.bind(this);
    this.submit=this.submit.bind(this);
  }
  changeState(event){
    this.setState({
      [event.target.name]:event.target.value
    });
  }
  submit(e){
    e.preventDefault();
    const {shipmentId,origin,destination,token}=this.state;
    let newShipment={"shipmentId":shipmentId,"origin":origin,"destination":destination};
    axios.post('http://0.0.0.0:5000/new_shipment',newShipment,{auth:{username:token}}).then(
      (result)=>{
        console.log(result.status);
        if (result.status==201){
          console.log("Shipment Created")
        }
      }
    )
  }

  render(){
    return(
      <>
        <div className="content">
          <Row>
            <Col md="8">
              <Card>
                <CardHeader>
                  <h5 className="title">Create New Shipment</h5>
                </CardHeader>
                <CardBody>
                  <Form onSubmit={this.submit}>
                    <Row>
                      <Col className="pr-md-1" md="5">
                        <FormGroup>
                          <label>Shipment ID</label>
                          <Input
                            name="shipmentId"
                            type="text"
                            onChange={this.changeState}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col className="pr-md-1" md="8">
                        <FormGroup>
                          <label>Origin</label>
                          <Input
                            name="origin"
                            type="text"
                            onChange={this.changeState}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col className="pr-md-1" md="8">
                        <FormGroup>
                          <label>Destination</label>
                          <Input
                            name="destination"
                            type="text"
                            onChange={this.changeState}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Button className="btn-fill" color="secondary" type="submit">
                      Create Shipment
                    </Button>
                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </>
    );
  }
}
export default NewShipment;
