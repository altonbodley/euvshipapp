import React from "react";
import axios from 'axios';
import { Redirect } from "react-router-dom";
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardText,
  FormGroup,
  Form,
  Input,
  Row,
  Col
} from "reactstrap";

class WRD extends React.Component{
  constructor(props){
    super(props)
    this.state={token:sessionStorage.getItem("token"),shipper:"",date:'',inLandCarrier:'',receiptNo:''};
    this.changeState=this.changeState.bind(this);
    this.submit=this.submit.bind(this);
  }
  changeState(event){
    this.setState({
      [event.target.name]:event.target.value
    });
  }
  submit(e){
    e.preventDefault()
    console.log(this.state.shipper);
    this.props.shipper=this.state.shipper
  }

  render(){
    return(
      <>
        <div className="content">
          <Row>
            <Col md="8">
              <Card>
                <CardHeader>
                  <h5 className="title"> Warehouse Receipt Details</h5>
                </CardHeader>
                <CardBody>
                  <Form onSubmit={this.submit}>
                  <Row>
                    <Col className="pr-md-1" md="5">
                      <FormGroup>
                        <label>Receipt Number</label>
                        <Input
                          name="receiptNo"
                          type="text"
                          onChange={this.changeState}
                        />
                      </FormGroup>
                    </Col>
                    <Col className="pr-md-1" md="7">
                      <FormGroup>
                        <label>Date</label>
                        <Input
                          name="date"
                          type="Date"
                          onChange={this.changeState}
                        />
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col className="pr-md-1" md="7">
                      <FormGroup>
                        <label>Inland Carrier</label>
                        <Input
                          name="inLandCarrier"
                          type="text"
                          onChange={this.changeState}
                        />
                      </FormGroup>
                    </Col>
                    <Col className="pr-md-1" md="7">
                      <FormGroup>
                        <label>Shipper</label>
                        <Input
                          name="shipper"
                          type="text"
                          onChange={this.changeState}
                        />
                      </FormGroup>
                    </Col>
                  </Row>
                  <Button className="btn-fill" color="secondary" onClick={this.props.data(this.state)}>
                    Add WRD details
                  </Button>
                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </>
    )
  }
}
export default WRD;
