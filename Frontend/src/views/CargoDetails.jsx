import React from "react";
import axios from 'axios';
import { Redirect } from "react-router-dom";
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardText,
  FormGroup,
  Form,
  Input,
  Row,
  Col
} from "reactstrap";

class CargoDetails extends React.Component{
  constructor(props){
    super(props)
    this.state={token:sessionStorage.getItem("token"),pieces:'',cargoType:'',weight:'',length:'',width:'',height:'',cubic:'',receiptNo:''};
    this.changeState=this.changeState.bind(this);
    this.submit=this.submit.bind(this);
  }
  changeState(event){
    this.setState({
      [event.target.name]:event.target.value
    });
  }
  submit(e){
    e.preventDefault()
  }
  render(){
    return(
      <>
        <div className="content">
          <Row>
            <Col md="8">
              <Card>
                <CardHeader>
                  <h5 className="title">Cargo Details</h5>
                </CardHeader>
                <CardBody>
                  <Form onSubmit={this.submit}>
                    <Row>
                      <Col className="pr-md-1" md="5">
                        <FormGroup>
                          <label>Pieces</label>
                          <Input
                            name="shipmentId"
                            type="text"
                            onChange={this.changeState}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col className="pr-md-1" md="2">
                        <FormGroup>
                          <label>Length</label>
                          <Input
                            name="length"
                            type="text"
                            onChange={this.changeState}
                          />
                        </FormGroup>
                      </Col>
                      <Col className="pr-md-1" md="2">
                        <FormGroup>
                          <label>Width</label>
                          <Input
                            name="width"
                            type="text"
                            onChange={this.changeState}
                          />
                        </FormGroup>
                      </Col>
                      <Col className="pr-md-1" md="2">
                        <FormGroup>
                          <label>Height</label>
                          <Input
                            name="height"
                            type="text"
                            onChange={this.changeState}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col className="pr-md-1" md="6">
                        <FormGroup>
                          <label>Cargo Type</label>
                          <Input
                            name="cargoType"
                            type="text"
                            onChange={this.changeState}
                          />
                        </FormGroup>
                      </Col>
                      <Col className="pr-md-1" md="6">
                        <FormGroup>
                          <label>Weight</label>
                          <Input
                            name="weight"
                            type="text"
                            onChange={this.changeState}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col className="pr-md-1" md="6">
                        <FormGroup>
                          <label>Cubic </label>
                          <Input
                            name="cubic"
                            type="text"
                            onChange={this.changeState}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Button className="btn-fill" color="secondary" type="submit">
                      Add Cargo Details
                    </Button>
                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </>
    )
  }
}
export default CargoDetails;
