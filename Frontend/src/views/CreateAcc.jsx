import React from "react";
import axios from 'axios';
import { Redirect } from "react-router-dom";

// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardText,
  FormGroup,
  Form,
  Input,
  Row,
  Col
} from "reactstrap";

class CreateAcc extends React.Component {
  constructor(props){
    super(props);
    this.state={error:false, username:'',password:'', email:'',
      fname:'',lname:'',addressl1:'', addressl2:'',city:'',
      parish:'',contact:'',trn:'', dpt:'',job:'',token:sessionStorage.getItem("token")
    };
    this.changeState=this.changeState.bind(this);
    this.submit=this.submit.bind(this);
  }
  validateForm(){
    return this.state.username.length >5 && this.state.password.length>0
  }
  changeState(event){
    this.setState({
      [event.target.name]:event.target.value
    });
  }
  submit(e){
    e.preventDefault();
    const {username,password,email,fname,lname,addressl1,addressl2,
      city,parish,contact,trn,dpt,job,token}=this.state;
    let emp_data={"username":username,"email":email,"fname":fname,"lname":lname,"addressl1":addressl1,"addressl2":addressl2,
      "city":city,"parish":parish,"contact":contact,"trn":trn,"dpt":dpt,"job":job
    }
    let usr_data={"username":username,"password":password,"role":"employee"}

    console.log(emp_data,usr_data)

    axios.post('http://0.0.0.0:5000/new_User',usr_data,{auth:{username:token}}).then(
      (result)=>{
        console.log(result.status);
        if(result.status==201){
          console.log("1down")
          console.log(token)
          axios.post('http://0.0.0.0:5000/new_employee',emp_data,{auth:{username:token}}).then(
            result=>{
              if(result.status!=201){
                this.setState({error:true})
              }else{
                console.log("Success")
                this.props.history.push("/euvasal/employees");
              }
            }
          )
        }else{
          this.setState({error:true})
        }
      }
    )
  }

  render(){
    return(
      <>
        <div className="content">
          <Row>
            <Col md="8">
              <Card>
                <CardHeader>
                  <h5 className="title">Create Employee Account</h5>
                </CardHeader>
                <CardBody>
                  <Form onSubmit={this.submit}>
                    <Row>
                      <Col className="px-md-1" md="3">
                        <FormGroup>
                          <label>Username</label>
                          <Input
                            name="username"
                            type="text"
                            onChange={this.changeState}
                          />
                        </FormGroup>
                      </Col>
                      <Col className="pr-md-1" md="5">
                        <FormGroup>
                          <label>password</label>
                          <Input
                            name="password"
                            type="password"
                            onChange={this.changeState}
                          />
                        </FormGroup>
                      </Col>
                      <Col className="pl-md-1" md="4">
                        <FormGroup>
                          <label>Email address</label>
                          <Input
                            name="email"
                            type="email"
                            onChange={this.changeState}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col className="pr-md-1" md="6">
                        <FormGroup>
                          <label>First Name</label>
                          <Input
                            name="fname"
                            type="text"
                            onChange={this.changeState}
                          />
                        </FormGroup>
                      </Col>
                      <Col className="pl-md-1" md="6">
                        <FormGroup>
                          <label>Last Name</label>
                          <Input
                            name="lname"
                            onChange={this.changeState}
                            type="text"
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col md="12">
                        <FormGroup>
                          <label>Address line 1</label>
                          <Input
                            type="text"
                            name="addressl1"
                            onChange={this.changeState}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col md="12">
                        <FormGroup>
                          <label>Address line 2</label>
                          <Input
                            type="text"
                            name="addressl2"
                            onChange={this.changeState}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col className="pr-md-1" md="4">
                        <FormGroup>
                          <label>City</label>
                          <Input
                            name="city"
                            onChange={this.changeState}
                            type="text"
                          />
                        </FormGroup>
                      </Col>
                      <Col className="px-md-1" md="7">
                        <FormGroup>
                          <label>Parish</label>
                          <Input
                            type="text"
                            name="parish"
                            onChange={this.changeState}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col className="pl-md-1" md="4">
                        <FormGroup>
                          <label>Contact</label>
                          <Input
                            type="number"
                            name="contact"
                            onChange={this.changeState}
                          />
                        </FormGroup>
                      </Col>
                      <Col md="4">
                        <FormGroup>
                          <label>TRN</label>
                          <Input
                            type="number"
                            name="trn"
                            onChange={this.changeState}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col className="pl-md-1" md="6">
                        <FormGroup>
                          <label>Department</label>
                          <Input
                            name="dpt"
                            type="text"
                            onChange={this.changeState}
                          />
                        </FormGroup>
                      </Col>
                      <Col className="pl-md-1" md="6">
                        <FormGroup>
                          <label>Job Title</label>
                          <Input
                            name="job"
                            type="text"
                            onChange={this.changeState}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Button className="btn-fill" color="secondary" type="submit">
                      Create Employee
                    </Button>
                  </Form>
                  <Row >
                    <ul>
                      <li id={this.state.error ? "errorVis":"error"}>This form has errors</li>
                    </ul>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </>
    );
  }
}
export default CreateAcc;
