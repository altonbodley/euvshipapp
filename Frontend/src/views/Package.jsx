import React from "react";
import axios from 'axios';
import { Redirect } from "react-router-dom";
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardText,
  FormGroup,
  Form,
  Input,
  Row,
  Col
} from "reactstrap";
import WRD from "./WRD.jsx"
import CargoDetails from "./CargoDetails.jsx"
import PackageDetails from "./PackageDetails.jsx"

class Package extends React.Component{
  constructor(props){
    super(props)
    this.state={token:sessionStorage.getItem("token"),receiptNo:'',trNo:'',shipmentId:'',fname:'',lname:'',shipper:'',date:'',inLandCarrier:''};
    this.changeState=this.changeState.bind(this);
  }
  changeState(event){
    this.setState({
        [event.target.name]:event.target.value
      });
  }

  stateUpdate(value){
      return ()=>{
        console.log(value);
      this.setState({shipper:value.shipper,date:value.date,inLandCarrier:value.inLandCarrier});
      console.log(this.state.shipper);
    }
  }
  packageAdd(value){}

render(){
    return(
      <>
      <div className="content">
        <Row>
          <Col md="8">
            <Card>
              <CardHeader>
                <h5 className="title">New Package</h5>
              </CardHeader>
              <CardBody>
                <Form onSubmit={this.submit}>
                  <Row>
                    <Col className="pr-md-1" md="5">
                      <FormGroup>
                        <label>Receipt Number</label>
                        <Input
                          name="receiptNo"
                          type="text"
                          onChange={this.changeState}
                        />
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col className="pr-md-1" md="8">
                      <FormGroup>
                        <label>Tracking Number</label>
                        <Input
                          name="trNo"
                          type="text"
                          onChange={this.changeState}
                        />
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col className="pr-md-1" md="8">
                      <FormGroup>
                        <label>Shipment ID</label>
                        <Input
                          name="shipmentId"
                          type="text"
                          onChange={this.changeState}
                        />
                      </FormGroup>
                    </Col>
                    <Col className="pr-md-1" md="8">
                      <FormGroup>
                        <label>Customer's First Name</label>
                        <Input
                          name="fname"
                          type="text"
                          onChange={this.changeState}
                        />
                      </FormGroup>
                    </Col>
                    <Col className="pr-md-1" md="8">
                      <FormGroup>
                        <label>Customer's Last Name</label>
                        <Input
                          name="lname"
                          type="text"
                          onChange={this.changeState}
                        />
                      </FormGroup>
                    </Col>
                  </Row>
                  <Button className="btn-fill" color="secondary" onClick={this.packageAdd}>
                    Add New Package
                  </Button>
                </Form>
                <WRD data={this.stateUpdate.bind(this)}/>
                <CargoDetails/>
              </CardBody>
            </Card>
          </Col>
          <Col md="4">
            <PackageDetails shipmentId={this.state.shipmentId} shipper={this.state.shipper}
            date={this.state.date} inLandCarrier={this.state.inLandCarrier} receiptNo={this.state.receiptNo}
            trNo={this.state.trNo} customer={this.state.fname + " "+ this.state.lname} />
          </Col>
        </Row>
      </div>
      </>
    )
  }
}
export default Package;
