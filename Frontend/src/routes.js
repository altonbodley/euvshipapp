import Dashboard from "views/Dashboard.jsx";
import Icons from "views/Icons.jsx";
import Map from "views/Map.jsx";
import Notifications from "views/Notifications.jsx";
import Rtl from "views/Rtl.jsx";
import TableList from "views/TableList.jsx";
import Typography from "views/Typography.jsx";
import UserProfile from "views/UserProfile.jsx";
import NewCustomer from "views/NewCustomer.jsx"
import NewShipment from "views/NewShipment.jsx"
import Customer from "views/Customer.jsx";
import Employees from "views/Employees.jsx";
import Login from "views/Login.jsx";
import CreateAcc from "views/CreateAcc.jsx";
import Package from "views/Package.jsx"

var routes = [
  {
    path:"/login",
    name:"Login",
    icon: "tim-icons icon-key-25",
    component: Login,
    layout: "/euvasal"
  },
  {
    path: "/employees",
    name: "Employees",
    icon: "tim-icons icon-pencil",
    component: Employees,
    layout: "/euvasal"
  },
  {
    path:"/create_account",
    name:"New Employee",
    icon: "tim-icons icon-single-02",
    component: CreateAcc,
    layout: "/euvasal"
  },
  {
    path: "/customer",
    name: "Customer",
    icon: "tim-icons icon-pencil",
    component: Customer,
    layout: "/euvasal"
  },
  {
    path: "/new_customer",
    name: "New Customer",
    icon: "tim-icons icon-pencil",
    component: NewCustomer,
    layout: "/euvasal"
  },
  {
    path: "/new_shipment",
    name: "Add New Shipment",
    icon: "tim-icons icon-notes",
    component: NewShipment,
    layout: "/euvasal"
  },
  {
    path:"/package",
    name:"New Package",
    icon: "tim-icons icon-app",
    component: Package,
    layout: "/euvasal"
  },
  {
    path: "/dashboard",
    name: "Dashboard",
    rtlName: "لوحة القيادة",
    icon: "tim-icons icon-chart-pie-36",
    component: Dashboard,
    layout: "/euvasal"
  }

];
export default routes;
