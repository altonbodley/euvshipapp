import React from "react";
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import { Router, Route, Switch, Redirect } from "react-router-dom";

import AdminLayout from "layouts/Admin/Admin.jsx";
import RTLLayout from "layouts/RTL/RTL.jsx";

import { Provider as AlertProvider } from 'react-alert';
import AlertTemplate from 'react-alert-template-basic';
import NewCustomer from "views/NewCustomer.jsx"


import "assets/scss/black-dashboard-react.scss";
import "assets/demo/demo.css";
import "assets/css/nucleo-icons.css";
import "assets/css/custom.css";

var auth =sessionStorage.getItem("auth");
console.log(typeof(auth));
console.log(auth);
const hist = createBrowserHistory();
const options = {
  timeout: 3000,
  position: "top center"
};

ReactDOM.render(
  <Router history={hist}>
    <div>
      <AlertProvider template={AlertTemplate} {...options}>
        
        <Switch>
          <Route path="/euvasal" render={props => <AdminLayout {...props} />} />
          <Route path="/rtl" render={props => <RTLLayout {...props} />} />
          {auth=='true'? (<Redirect from="/" to="euvasal/dashboard" />):
          (<Redirect from="/" to="euvasal/login" />)
          }
        </Switch>
      </AlertProvider>
    </div>
  </Router>,
  document.getElementById("root")
);
