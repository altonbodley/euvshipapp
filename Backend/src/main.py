from flask import Flask,jsonify,request,abort,g
from flask_httpauth import HTTPBasicAuth
from sqlalchemy import and_
from .entities.entity import Session,engine,Base
from .entities.users import *
from .entities.employee import *
from .entities.comments import *
from .entities.customers import *
from .entities.package import *
from .entities.shipment import *
from .entities.wrd import *
from .entities.cargoDetails import *
import json


app=Flask(__name__)
#generate DB schema
Base.metadata.create_all(engine)
auth=HTTPBasicAuth()

@app.route('/login',methods=['POST'])
def login():
    ses=Session()
    data=request.get_json()
    user=ses.query(User).filter_by(username=data['username']).first()
    if not user or not user.verify_password(data['password']):
        ses.close()
        return jsonify({"error":"FailedAuth"})
    ses.close()
    return user.generate_auth_token(),202



@app.route('/users')
@auth.login_required
def get_users():
    AdminAuthorized()
    #Start session
    session=Session()
    user=UserSchema().load(request.get_json())
    #check for existing updated_at
    users=session.query(User).all()
    #transforming into JSON-serializable objects
    schema=UserSchema(many=True)
    user=schema.dump(users)
    #serializeing as json
    session.close()
    return jsonify(user.data)

@app.route('/new_User',methods=['POST'])
@auth.login_required
def add_user():
    # AdminAuthorized()
    ses=Session()
    #mount user object
    new_user=UserSchema(only=('username','password','role')).load(request.get_json())
    print(new_user.data)
    if ses.query(User).filter_by(username=new_user.data['username']).first() is not None:
        ses.close()
        abort(400)
    user=User(**new_user.data,created_by=g.user.username)
    user.hash_password(user.password)
    #Persist user
    ses.add(user)
    ses.commit()
    ses.close()
    # return created users
    # newUser=UserSchema().dump(user).data
    return jsonify({"data":"success"}),201

@app.route('/new_employee',methods=['POST'])
@auth.login_required
def new_emp():
    # AdminAuthorized()
    ses=Session()
    newEmp=EmployeeSchema(only=('email','fname','lname','addressl1','addressl2','city','parish','contact','trn','dpt','job','username')).load(request.get_json())
    print(newEmp.data['email'])
    print (newEmp.data['trn'])
    if ses.query(Employee).filter_by(email=newEmp.data['email']).first() is not None:
        abort(400)
    nEmp=Employee(**newEmp.data,created_by=g.user.username)
    ses.add(nEmp)
    ses.commit()
    ses.close()
    print("EMP CREATED")
    return jsonify({"data":"Success"}),201

@app.route('/get_employees',methods=["GET"])
@auth.login_required
def get_employees():
    # AdminAuthorized()
    session=Session()
    emps=session.query(Employee).all()
    print(emps)
    employeeS=EmployeeSchema(many=True)
    allEmps=employeeS.dump(emps)
    session.close()
    return jsonify(allEmps.data),200

@app.route('/new_customer',methods=['POST'])
@auth.login_required
def new_customer():
    EmpAuthorized()
    session=Session()
    new_cust=CustomerSchema(only=('email','fname','lname','addressl1','addressl2','city','parish','contact','trn')).load(request.get_json())
    if session.query(Customer).filter_by(email=new_cust.data['email']).first() is not None:
        session.close()
        abort(400)
    print ("TRN:")
    print (new_cust.data['trn'])
    print(new_cust.data)
    newCust=Customer(**new_cust.data,created_by=g.user.username)
    session.add(newCust)
    session.commit()
    session.close()
    return jsonify({"data":"Success"}),201

@app.route('/get_all_customers',methods=['GET'])
@auth.login_required
def get_customer():
    EmpAuthorized()
    cust=CustomerSchema().load(request.get_json())
    ses=Session()
    customer=ses.query(Customer.fname,Customer.lname,Customer.email,Customer.addressl1,Customer.addressl2,Customer.city,Customer.parish,Customer.contact,Customer.trn).all()
    print (customer)
    custSchema=CustomerSchema(many=True)
    custNames=custSchema.dump(customer)
    ses.close()
    return jsonify(custNames.data),200

@app.route('/new_shipment',methods=['POST'])
@auth.login_required
def new_shipment():
    EmpAuthorized()
    session=Session()
    new_shipment=ShipmentSchema(only=('shipmentId','origin','destination')).load(request.get_json())
    if session.query(Shipment).filter_by(shipmentId=new_shipment.data['shipmentId']).first() is not None:
        session.close()
        abort(400)
    newShipment=Shipment(**new_shipment.data,created_by=g.user.username)
    session.add(newShipment)
    session.commit()
    session.close()
    return jsonify({'data':"Success"}),201

@app.route('/get_shipment',methods=['GET'])
@auth.login_required
def get_shipments():
    EmpAuthorized()
    session=Session()
    # ship=ShipmentSchema().load(request.get_json())
    shipments=session.query(Shipment.shipmentId,Shipment.origin,Shipment.destination).all()
    shipSchema=ShipmentSchema(many=True)
    shipmnts=shipSchema.dump(shipments)
    session.close()
    return jsonify(shipments.data),200


@app.route('/new_package',methods=["POST"])
@auth.login_required
def new_package():
    EmpAuthorized()
    session=Session()
    new_package=PackageSchema(only=('receiptNo','trNo','shipmentId')).load(request.get_json())
    name=PackageSchema(only=('fname','lname')).load(request.get_json())
    print (f"New packsage: {new_package}")
    custId=session.query(Customer.id).filter(and_(Customer.fname==name.data['fname'],Customer.lname==name.data['lname']))
    if custId is None:
        abort(400)#Customer Doesn't exist
    if session.query(Package).filter_by(receiptNo=new_package.data['receiptNo']).first() is not None:
        session.close()
        abort(400)
    newPackage=Package(**new_package.data,cId=custId,created_by=g.user.username)
    session.add(newPackage)
    session.commit()
    session.close()
    return jsonify({'data':'Success'}),201

@auth.verify_password
def verify_password(username,password):
    print(f"username: {username}")
    ses=Session()
    #Token auth_token
    user=User.verify_auth_token(username)
    if type(user) is str:
        user=ses.query(User).filter_by(username=username).first()
        if  not user or not user.verify_password(password):
            ses.close()
            return False
    g.user=user
    ses.close()
    return True

def EmpAuthorized():
    if g.user.role not in ["admin","Administrator","employee"]:
        abort(401)
    return True
def AdminAuthorized():
    if g.user.role not in ["admin","Administrator"]:
        abort(401)
    return True

@app.after_request
def after_request(response):
    response.headers["Cache-Control"] = "no-cache, no-store, must-revalidate, public, max-age=0"
    response.headers["Expires"] = 0
    response.headers["Pragma"] = "no-cache"
    return response
