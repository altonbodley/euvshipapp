from marshmallow import Schema,fields
from sqlalchemy import Column,String,Integer,ForeignKey
from .entity import Entity, Base

class Package(Entity,Base):
    __tablename__='package'
    id=Column(Integer(),autoincrement=True,primary_key=True)
    receiptNo=Column(Integer(),unique=True)
    trNo=Column(Integer())
    shipmentId=Column(Integer(),ForeignKey('shipment.shipmentId',onupdate="CASCADE",ondelete="CASCADE"),nullable=False)
    cId=Column(Integer(),ForeignKey('customer.id'),nullable=False)
    def __init__(self,receiptNo,trNo,shipmentId,cId,created_by):
        Entity.__init__(self,created_by)
        self.receiptNo=receiptNo
        self.trNo=trNo
        self.shipmentId=shipmentId
        self.cId=cId

class PackageSchema(Schema):
    id=fields.Number()
    receiptNo=fields.Number()
    trNo=fields.Number()
    shipmentId=fields.Number()
    cId=fields.Number()
    created_at=fields.DateTime()
    updated_at=fields.DateTime()
    last_updated_by=fields.Str()
    fname=fields.Str()
    lname=fields.Str()
