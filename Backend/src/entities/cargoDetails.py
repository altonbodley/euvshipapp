from marshmallow import Schema,fields
from sqlalchemy import Column,String,Integer,ForeignKey
from .entity import Entity, Base

class CargoDetails(Entity,Base):
    __tablename__="cargo_details"
    id=Column(Integer(),autoincrement=True,primary_key=True)
    pieces=Column(Integer())
    cargoType=Column(String(20))
    weight=Column(Integer())
    length=Column(Integer())
    width=Column(Integer())
    height=Column(Integer())
    cubic=Column(Integer())
    receiptNo=Column(Integer(),ForeignKey('package.receiptNo',ondelete="CASCADE",onupdate="CASCADE"))
    def __inti__(self,pieces,cargoType,weight,length,width,height,cubic,receiptNo,created_by):
        Entity.__init__(self,created_by)
        self.pieces=pieces
        self.cargoType=cargoType
        self.weight=weight
        self.length=length
        self.width=width
        self.height=height
        self.cubic=cubic
        self.receiptNo=receiptNo

class CargoDetailsSchema(Schema):
    id=fields.Number()
    pieces=fields.Number()
    cargoType=fields.Str()
    weight=fields.Number()
    length=fields.Number()
    width=fields.Number()
    height=fields.Number()
    cubic=fields.Number()
    receiptNo=fields.Number()
