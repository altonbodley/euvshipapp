from marshmallow import Schema,fields
from sqlalchemy import Column,String,Integer
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer, BadSignature, SignatureExpired
from passlib.apps import custom_app_context as pwd_context
from .entity import Entity, Base,Session


key='dbcwishkjcnhwio8641c8wecrjncwevc8w49/+46576@#$'
class User(Entity,Base):
    __tablename__='user'
    id=Column(Integer,autoincrement=True,primary_key=True)
    username=Column(String(20),unique=True)
    password=Column(String(225))
    role=Column(String(20))

    def __init__(self,username,password,role,created_by):
        Entity.__init__(self,created_by)
        self.username=username
        self.password=password
        self.role=role
    def hash_password(self,password):
        self.password=pwd_context.encrypt(password)
    def verify_password(self,password):
        print ("i ran")
        return pwd_context.verify(password,self.password)

    def generate_auth_token(self,expiration=6000):
        s=Serializer(key,expires_in=expiration)
        print (s.dumps({'id':self.id}))
        return s.dumps({'id':self.id})

    @staticmethod
    def verify_auth_token(auth_token):
        ses=Session()
        s=Serializer(key)
        try:
            data=s.loads(auth_token)
        except SignatureExpired:
            return "SignatureExpired"
        except BadSignature:
            return "BadSignature"
        user = ses.query(User).get(data['id'])
        ses.close()
        return user
class UserSchema(Schema):
    id=fields.Number()
    username=fields.Str()
    pasword=fields.Str()
    role=fields.Str()
    created_at=fields.DateTime()
    updated_at=fields.DateTime()
    last_updated_by=fields.Str()
