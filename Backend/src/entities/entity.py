from datetime import datetime
from sqlalchemy import create_engine,Column, String, Integer, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

db_url="0.0.0.0"
db_name="EuvShip"
db_user="root"
db_password=""
db_engine="mysql"
db_connector="pymysql"
engine=create_engine(f'{db_engine}+{db_connector}://{db_user}@{db_url}/{db_name}') #if db does not have a password
# engine=create_engine(f'{db_engine}+{db_connector}://{db_user}:{db_password}@{db_url}/{db_name}') #If DB has a password
Session=sessionmaker(bind=engine)

Base=declarative_base()

class Entity():
    id=Column(Integer,primary_key=True)
    created_at=Column(DateTime)
    updated_at=Column(DateTime)
    last_updated_by=Column(String(20))

    def __init__(self,created_by):
        self.created_at=datetime.now()
        self.updated_at=datetime.now()
        self.last_updated_by=created_by
