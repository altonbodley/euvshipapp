from marshmallow import Schema,fields
from sqlalchemy import Column,String,Integer,ForeignKey
from .entity import Entity, Base

class Employee(Entity,Base):
    __tablename__="employee"
    id=Column(Integer,autoincrement=True,primary_key=True)
    email=Column(String(30),unique=True)
    fname=Column(String(30))
    lname=Column(String(30))
    addressl1=Column(String(100))
    addressl2=Column(String(100))
    city=Column(String(50))
    parish=Column(String(50))
    contact=Column(Integer())
    trn=Column(Integer(),unique=True)
    dpt=Column(String(20))
    job=Column(String(70))
    username=Column(String(20),ForeignKey('user.username',onupdate="CASCADE", ondelete="CASCADE"),nullable=False,unique=True)
    def __init__(self,email,fname,lname,addressl1,addressl2,city,parish,contact,trn,dpt,job,username,created_by):
        Entity.__init__(self,created_by)
        self.email=email
        self.fname=fname
        self.lname=lname
        self.addressl1=addressl1
        self.addressl2=addressl2
        self.city=city
        self.parish=parish
        self.contact=contact
        self.trn=trn
        self.dpt=dpt
        self.job=job
        self.username=username

class EmployeeSchema(Schema):
    id=fields.Number()
    email=fields.Str()
    fname=fields.Str()
    lname=fields.Str()
    addressl1=fields.Str()
    addressl2=fields.Str()
    city=fields.Str()
    parish=fields.Str()
    contact=fields.Str()
    trn=fields.Number()
    dpt=fields.Str()
    job=fields.Str()
    username=fields.Str()
    created_at=fields.DateTime()
    updated_at=fields.DateTime()
    last_updated_by=fields.Str()
