from marshmallow import Schema,fields
from sqlalchemy import Column,String,Integer,ForeignKey
from .entity import Entity, Base

class Comments(Base,Entity):
    __tablename__='comments'
    id=Column(Integer(),autoincrement=True,primary_key=True)
    comment=Column(String(225))
    receiptNo=Column(Integer(),ForeignKey('package.receiptNo',onupdate="CASCADE",ondelete="CASCADE"),nullable=False)
    def __init__(self,comment,receiptNo,created_by):
        Entity(self,created_by)
        self.comment=comment
        self.receiptNo=receiptNo

class CommentsSchema(Schema):
    id=fields.Number()
    comment=fields.Str()
    receiptNo=fields.Number()
    created_at=fields.DateTime()
    updated_at=fields.DateTime()
    last_updated_by=fields.Str()
