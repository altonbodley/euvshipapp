from marshmallow import Schema,fields
from sqlalchemy import Column,String,Integer,DateTime,ForeignKey
from .entity import Entity, Base

class Wrd(Base,Entity):
    __tablename__='wrd'
    id=Column(Integer(),autoincrement=True,primary_key=True)
    shipper=Column(String(50))
    date=Column(DateTime)
    inLandCarrier=Column(String(50))
    receiptNo=Column(Integer(),ForeignKey('package.receiptNo',ondelete="CASCADE",onupdate="CASCADE"),nullable=False)
    def __init__(self,shipper,date,inLandCarrier,receiptNo,created_by):
        Entity.__init__(self,created_by)
        self.shipper=shipper
        self.date=date
        self.inLandCarrier=inLandCarrier
        self.receiptNo=receiptNo

class WrdSchema(Schema):
    id=fields.Number()
    shipper=fields.Str()
    date=fields.DateTime()
    inLandCarrier=fields.Str()
    receiptNo=fields.Number()
    created_at=fields.DateTime()
    updated_at=fields.DateTime()
    last_updated_by=fields.Str()
