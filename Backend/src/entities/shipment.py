from marshmallow import Schema,fields
from sqlalchemy import Column,String,Integer
from .entity import Entity, Base

class Shipment(Entity,Base):
    __tablename__="shipment"
    id=Column(Integer,autoincrement=True,primary_key=True)
    shipmentId=Column(Integer(),unique=True)
    origin=Column(String(70))
    destination=Column(String(70))
    def __init__(self,shipmentId,origin,destination,created_by):
        Entity.__init__(self,created_by)
        self.shipmentId=shipmentId
        self.origin=origin
        self.destination=destination

class ShipmentSchema(Schema):
    id=fields.Number()
    shipmentId=fields.Number()
    origin=fields.Str()
    destination=fields.Str()
